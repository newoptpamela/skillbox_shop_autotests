package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage {
    private WebDriver driver;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    private By productNameField = By.id("product_name");
    private By productPriceField = By.id("product_price");
    private By checkoutButton = By.id("checkout");

    public void enterProductName(String productName) {
        driver.findElement(productNameField).sendKeys(productName);
    }

    public void enterProductPrice(String productPrice) {
        driver.findElement(productPriceField).sendKeys(productPrice);
    }

    public void clickCheckoutButton() {
        driver.findElement(checkoutButton).click();
    }

    public boolean isSuccessMessageDisplayed() {
        return driver.getPageSource().contains("Order placed successfully");
    }

    public boolean isErrorMessageDisplayed() {
        return driver.getPageSource().contains("Order placement failed");
    }
}

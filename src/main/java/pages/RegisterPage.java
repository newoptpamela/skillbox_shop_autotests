package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {
    private WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    private By emailField = By.id("email");
    private By usernameField = By.id("username");
    private By passwordField = By.id("password");
    private By registerButton = By.id("register");

    public void enterEmail(String email) {
        driver.findElement(emailField).sendKeys(email);
    }

    public void enterUsername(String username) {
        driver.findElement(usernameField).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(passwordField).sendKeys(password);
    }

    public void clickRegisterButton() {
        driver.findElement(registerButton).click();
    }

    public boolean isSuccessMessageDisplayed() {
        return driver.getPageSource().contains("Registration successful");
    }

    public boolean isErrorMessageDisplayed() {
        return driver.getPageSource().contains("Registration failed");
    }
}

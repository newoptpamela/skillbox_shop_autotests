Feature: Checkout functionality

  Scenario: Successful checkout with valid product details
    Given User is on the checkout page
    When User enters valid product name and price
    And User clicks on checkout button
    Then User should see order success message

  Scenario: Unsuccessful checkout with invalid product details
    Given User is on the checkout page
    When User enters invalid product name and price
    And User clicks on checkout button
    Then User should see order error message

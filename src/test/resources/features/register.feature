Feature: Registration functionality

  Scenario: Successful registration with valid credentials
    Given User is on the registration page
    When User enters valid email, username, and password
    And User clicks on register button
    Then User should see registration success message

  Scenario: Unsuccessful registration with invalid credentials
    Given User is on the registration page
    When User enters invalid email, username, and password
    And User clicks on register button
    Then User should see registration error message

package steps;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.RegisterPage;
import static org.junit.Assert.assertTrue;

public class RegisterSteps {
    WebDriver driver = new FirefoxDriver();
    RegisterPage registerPage = new RegisterPage(driver);

    @Given("User is on the registration page")
    public void user_is_on_the_registration_page() {
        driver.get("http://intershop5.skillbox.ru/register/");
    }

    @When("User enters valid email, username, and password")
    public void user_enters_valid_email_username_and_password() {
        registerPage.enterEmail("newUser@example.com");
        registerPage.enterUsername("newUser");
        registerPage.enterPassword("newPassword");
    }

    @When("User enters invalid email, username, and password")
    public void user_enters_invalid_email_username_and_password() {
        registerPage.enterEmail("invalidEmail");
        registerPage.enterUsername("invalidUsername");
        registerPage.enterPassword("short");
    }

    @And("User clicks on register button")
    public void user_clicks_on_register_button() {
        registerPage.clickRegisterButton();
    }

    @Then("User should see registration success message")
    public void user_should_see_registration_success_message() {
        assertTrue("Registration successful message not found", registerPage.isSuccessMessageDisplayed());
    }

    @Then("User should see registration error message")
    public void user_should_see_registration_error_message() {
        assertTrue("Registration failed message not found", registerPage.isErrorMessageDisplayed());
    }
}

package steps;

import io.cucumber.java.en.*;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.LoginPage;
import static org.junit.Assert.assertTrue;

public class LoginSteps {

    WebDriver driver = new FirefoxDriver();
    LoginPage loginPage = new LoginPage(driver);

    @Step("User is on the login page")
    @Given("User is on the login page")
    public void user_is_on_the_login_page() {
        driver.get("http://intershop5.skillbox.ru/my-account/");
    }

    @Step("User enters valid username and password")
    @When("User enters valid username and password")
    public void user_enters_valid_username_and_password() {
        loginPage.enterUsername("validUsername");
        loginPage.enterPassword("validPassword");
    }

    @Step("User enters invalid username and password")
    @When("User enters invalid username and password")
    public void user_enters_invalid_username_and_password() {
        loginPage.enterUsername("invalidUsername");
        loginPage.enterPassword("invalidPassword");
    }

    @Step("User clicks on login button")
    @And("User clicks on login button")
    public void user_clicks_on_login_button() {
        loginPage.clickLoginButton();
    }

    @Step("User should be redirected to the home page")
    @Then("User should be redirected to the home page")
    public void user_should_be_redirected_to_the_home_page() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlToBe("http://intershop5.skillbox.ru/my-account/"));
    }

    @Step("User should see an error message")
    @Then("User should see an error message")
    public void user_should_see_an_error_message() {
        assertTrue("Expected error message not found", loginPage.isErrorMessageDisplayed());
    }
}

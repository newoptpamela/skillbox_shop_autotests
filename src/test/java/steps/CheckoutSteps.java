package steps;

import io.cucumber.java.en.*;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.CheckoutPage;
import static org.junit.Assert.assertTrue;

public class CheckoutSteps {
    WebDriver driver = new FirefoxDriver();
    CheckoutPage checkoutPage = new CheckoutPage(driver);

    @Step("User is on the checkout page")
    @Given("User is on the checkout page")
    public void user_is_on_the_checkout_page() {
        driver.get("http://intershop5.skillbox.ru/checkout/");
    }

    @Step("User enters valid product name and price")
    @When("User enters valid product name and price")
    public void user_enters_valid_product_name_and_price() {
        checkoutPage.enterProductName("Valid Product");
        checkoutPage.enterProductPrice("100");
    }

    @Step("User enters invalid product name and price")
    @When("User enters invalid product name and price")
    public void user_enters_invalid_product_name_and_price() {
        checkoutPage.enterProductName("");
        checkoutPage.enterProductPrice("-100");
    }

    @Step("User clicks on checkout button")
    @And("User clicks on checkout button")
    public void user_clicks_on_checkout_button() {
        checkoutPage.clickCheckoutButton();
    }

    @Step("User should see order success message")
    @Then("User should see order success message")
    public void user_should_see_order_success_message() {
        assertTrue("Order placed successfully message not found", checkoutPage.isSuccessMessageDisplayed());
    }

    @Step("User should see order error message")
    @Then("User should see order error message")
    public void user_should_see_order_error_message() {
        assertTrue("Order placement failed message not found", checkoutPage.isErrorMessageDisplayed());
    }
}
